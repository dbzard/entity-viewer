entity-viewer
============

Entity viewer plugin(web fragment module) for Spring based web application.

为Spring项目提供的实体管理插件


前提条件
--------
1. 请确认你的项目使用的是最新版本的Spring、SpringMVC以及JPA2

使用方法
--------
1. 到 https://bitbucket.org/dbzard/entity-viewer/downloads 下载最新的spring-entity-viewer-x.x.x.jar
2. 将此jar包放在你项目WEB-INF/lib文件夹下
3. 添加'classpath*:META-INF/entityviewer-context.xml'到DispatcherServlet的contextConfigLocation
   
   如下所示:
```
  <servlet>
		<servlet-name>appServlet</servlet-name>
		<servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
		<init-param>
			<param-name>contextConfigLocation</param-name>
			<param-value>/WEB-INF/spring/appServlet/your-context.xml,classpath*:META-INF/entityviewer-context.xml</param-value>
		</init-param>
		<load-on-startup>1</load-on-startup>
	</servlet>
```

 访问你项目的 /entityviewer 路径，完成!

* * *

Prerequisite
------------
1. Make sure your project use the latest Spring & Spring MVC & JPA2

Usage
-----
1. Go to https://bitbucket.org/dbzard/entity-viewer/downloads to get the latest spring-entity-viewer-x.x.x.jar.
2. Put the jar file in your project's WEB-INF/lib folder.
3. Add 'classpath*:META-INF/entityviewer-context.xml' to DispatcherServlet's contextConfigLocation.

   It should be like this:
```
  <servlet>
		<servlet-name>appServlet</servlet-name>
		<servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
		<init-param>
			<param-name>contextConfigLocation</param-name>
			<param-value>/WEB-INF/spring/appServlet/your-context.xml,classpath*:META-INF/entityviewer-context.xml</param-value>
		</init-param>
		<load-on-startup>1</load-on-startup>
	</servlet>
```

 Navigate to /entityviewer, done!



dbzard@gmail.com