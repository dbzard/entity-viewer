package com.dbzard.entityadmintest.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Child extends Parent {

	@Column
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
