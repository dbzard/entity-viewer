package com.dbzard.entityviewer.model;

public class EntityModel {
	private String id;

	private String name;

	private boolean haveParent;

	private boolean haveChild;

	public EntityModel(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isHaveParent() {
		return haveParent;
	}

	public void setHaveParent(boolean haveParent) {
		this.haveParent = haveParent;
	}

	public boolean isHaveChild() {
		return haveChild;
	}

	public void setHaveChild(boolean haveChild) {
		this.haveChild = haveChild;
	}
}
