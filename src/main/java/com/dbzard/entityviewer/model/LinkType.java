package com.dbzard.entityviewer.model;

public enum LinkType {
	INHERIT, IMPLEMENT, ASSOCIATION
}
