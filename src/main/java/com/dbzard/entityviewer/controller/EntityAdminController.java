package com.dbzard.entityviewer.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dbzard.entityviewer.model.PageModel;
import com.dbzard.entityviewer.repository.EntityAdminRepository;

/**
 * Entityadmin's main controller.
 * 
 * @author dbzard
 */
@Controller
@RequestMapping("/entityviewer")
public class EntityAdminController {
	private static final Logger logger = LoggerFactory.getLogger(EntityAdminController.class);

	private static final String HOME_PAGE = "home.html";

	@Autowired
	private EntityAdminRepository repo;

	@RequestMapping(method = RequestMethod.GET)
	public String home(Model model) {
		logger.debug("Enter home.");
		
		return "redirect:/" + HOME_PAGE;
	}

	@RequestMapping(value = "/entities", method = RequestMethod.GET)
	@ResponseBody
	public PageModel getEntityList(@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer size, Model model) {
		logger.debug("Invoke getEntityList method.");

		if (page == null) {
			page = 1;
		}
		if (size == null) {
			size = 5;
		}
		
		Pageable pageable = new PageRequest(page - 1, size);
		PageModel entities = repo.getEntitiesName(pageable);
		return entities;
	}
}
