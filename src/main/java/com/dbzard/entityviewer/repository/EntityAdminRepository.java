package com.dbzard.entityviewer.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.dbzard.entityviewer.model.PageModel;

/**
 * Entityadmin's repository interface.
 * 
 * @author dbzard
 */
public interface EntityAdminRepository {
	public PageModel getEntitiesName(Pageable pageable);
	
	public List getEntityList(String entityName);
}
