package com.dbzard.entityviewer.repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.metamodel.EntityType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Component;

import com.dbzard.entityviewer.model.PageModel;

/**
 * @author dbzard
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
@Component
public class EntityAdminRepositoryImpl implements EntityAdminRepository {

	private static final Logger logger = LoggerFactory
			.getLogger(EntityAdminRepositoryImpl.class);

	private Set<String> entityList = new TreeSet<String>();

	private Map<String, SimpleJpaRepository> jpaRepositoryMap = new HashMap<String, SimpleJpaRepository>();

	private EntityManagerFactory emf;

	private EntityManager em;

	/**
	 * Fill 'jpaRepositoryMap' with entity's SimpleJpaRepository
	 * 
	 * @param emf
	 */
	@Autowired
	public EntityAdminRepositoryImpl(EntityManagerFactory emf) {
		this.emf = emf;
		this.em = emf.createEntityManager();
		for (EntityType<?> entityType : emf.getMetamodel().getEntities()) {
			entityList.add(entityType.getName());

			jpaRepositoryMap.put(
					entityType.getName(),
					new SimpleJpaRepository(JpaEntityInformationSupport
							.getMetadata(entityType.getJavaType(), em), em));
		}
	}

	@Override
	public PageModel getEntitiesName(Pageable pageable) {
		logger.debug("Entityadmin get entity name in entitymanager.");

		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		int totalSize = entityList.size();
		int start = pageNumber * pageSize;
		int end = (pageNumber + 1) * pageSize;
		if (end >= totalSize) {
			end = totalSize;
		}

		Page page = new PageImpl(new ArrayList(entityList).subList(start, end),
				pageable, totalSize);
		PageModel result = new PageModel(page.getContent());
		result.setHasNextPage(page.hasNextPage());
		result.setHasPrePage(page.hasPreviousPage());
		result.setPageNumber(page.getNumber() + 1);
		return result;
	}

	@Override
	public List getEntityList(String entityName) {
		logger.debug("Entityadmin get " + entityName + "'s content list.");

		SimpleJpaRepository repo = jpaRepositoryMap.get(entityName);
		if (repo == null) {
			return Collections.EMPTY_LIST;
		}
		return repo.findAll();
	}

	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public Map<String, SimpleJpaRepository> getJpaRepositoryMap() {
		return jpaRepositoryMap;
	}

	public void setJpaRepositoryMap(
			Map<String, SimpleJpaRepository> jpaRepositoryMap) {
		this.jpaRepositoryMap = jpaRepositoryMap;
	}

	public EntityManagerFactory getEmf() {
		return emf;
	}
}
