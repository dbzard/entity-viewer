(function() {

	"use strict";

	window.b2Vec2 = Box2D.Common.Math.b2Vec2;
	window.b2BodyDef = Box2D.Dynamics.b2BodyDef;
	window.b2Body = Box2D.Dynamics.b2Body;
	window.b2FixtureDef = Box2D.Dynamics.b2FixtureDef;
	window.b2Fixture = Box2D.Dynamics.b2Fixture;
	window.b2World = Box2D.Dynamics.b2World;
	window.b2MassData = Box2D.Collision.Shapes.b2MassData;
	window.b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape;
	window.b2CircleShape = Box2D.Collision.Shapes.b2CircleShape;
	window.b2DebugDraw = Box2D.Dynamics.b2DebugDraw;

	window.requestNextAnimationFrame = (function() {
		return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.msRequestAnimationFrame ||
		function(callback, element) { // Assume element is visible
			var self = this,
				start, finish;
			window.setTimeout(function() {
				start = +new Date();
				callback(start);
				finish = +new Date();
				self.timeout = 1000 / 60 - (finish - start);
			}, self.timeout);
		};
	})
	();

})();